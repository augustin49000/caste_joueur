

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- Base de données :  `caste_joueur`
--
CREATE DATABASE IF NOT EXISTS `caste_joueur` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `caste_joueur`;

-- --------------------------------------------------------

--
-- Structure de la table `caste`
--

CREATE TABLE `caste` (
  `Code_Caste` int(11) NOT NULL,
  `Description_Caste` varchar(10000) NOT NULL,
  `Points_Caste` int(11) NOT NULL,
  `Nom_Caste` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Structure de la table `joueur`
--

CREATE TABLE `joueur` (
  `Code_Joueur` int(11) NOT NULL,
  `Pseudo_Joueur` text NOT NULL,
  `Argent_Joueur` int(11) NOT NULL,
  `Caste_Joueur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour la table `caste`
--
ALTER TABLE `caste`
  ADD PRIMARY KEY (`Code_Caste`);

--
-- Index pour la table `joueur`
--
ALTER TABLE `joueur`
  ADD PRIMARY KEY (`Code_Joueur`),
  ADD KEY `Caste_Joueur` (`Caste_Joueur`);

--
-- Contraintes pour la table `joueur`
--
ALTER TABLE `joueur`
  ADD CONSTRAINT `joueur_ibfk_1` FOREIGN KEY (`Caste_Joueur`) REFERENCES `caste` (`Code_Caste`);