<?php
doUpdateInsert();

function doUpdateInsert(){
    $contentInput = file_get_contents('php://input');

    $decodedContent = json_decode($contentInput);

    foreach($decodedContent->castes as $caste){
        //Check si la caste existe
        $sCheckCaste = "Select 1 From Caste where Code_Caste = :iCodeCaste";
        $bCasteExist = ExecSql($sCheckCaste, [":iCodeCaste" => $caste->Id]);
        

        if($bCasteExist[0][1] == '1'){
            //Update la caste
            $sRequeteCaste = "Update Caste 
            Set Nom_Caste = :sNomCaste,
            Description_Caste = :sDescriptionCaste,
            Points_Caste = :iPointsCaste
            Where Code_Caste = :iCodeCaste";

        }else{
            //Insert la caste
            $sRequeteCaste = "Insert Into Caste (Code_Caste, Nom_Caste, Description_Caste, Points_Caste)
            Values(:iCodeCaste, :sNomCaste, :sDescriptionCaste, :iPointsCaste)";
        }
        $aParams = [
            ":iCodeCaste" => $caste->Id,
            ":sNomCaste" => $caste->Nom,
            ":sDescriptionCaste" => $caste->Description,
            ":iPointsCaste" => $caste->Points
        ];
        ExecSql($sRequeteCaste, $aParams);
    }

    foreach($decodedContent->joueurs as $joueur){
        //Check si la caste existe
        $sCheckJoueur = "Select 1 From Joueur where Code_Joueur = :iCodeJoueur";
        $bJoueurExist = ExecSql($sCheckJoueur, [":iCodeJoueur" => $joueur->Id]);
        
        if($bJoueurExist[0][1] == 1){
            //Update la caste
            $sRequeteCaste = "Update Joueur 
            Set Pseudo_Joueur = :sPseudoJoueur,
            Argent_Joueur = :iArgentJoueur,
            Caste_Joueur = :iCasteJoueur
            Where Code_Joueur = :iCodeJoueur";

        }else{
            //Insert la caste
            $sRequeteCaste = "Insert Into Joueur (Code_Joueur, Pseudo_Joueur, Argent_Joueur, Caste_Joueur)
            Values(:iCodeJoueur, :sPseudoJoueur, :iArgentJoueur, :iCasteJoueur)";
        }
        $aParams = [
            ":sPseudoJoueur" => $joueur->Pseudo,
            ":iArgentJoueur" => $joueur->Argent,
            ":iCasteJoueur" => $joueur->Caste,
            ":iCodeJoueur" => $joueur->Id
        ];

        ExecSql($sRequeteCaste, $aParams);
    }

   
} 
function ExecSql(string $sRequete, array $aParam){
    try {
        $dsn = "mysql:host=localhost;dbname=caste_joueur";
        $utilisateur = "root";
        $motDePasse = "root";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $pdo = new PDO($dsn, $utilisateur, $motDePasse, $options);

        // Préparation de la requête SQL
        $stmt = $pdo->prepare($sRequete);

        // Exécution de la requête avec les paramètres fournis
        $stmt->execute($aParam);

        // Récupération des résultats si nécessaire
        // Exemple de récupération des résultats dans un tableau associatif
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Fermeture de la connexion
        $pdo = null;

        return $result;
    } catch (PDOException $e) {
        // Gérer les erreurs de la base de données
        // Vous pouvez personnaliser la gestion des erreurs selon vos besoins
        file_put_contents("ErreurSQL.txt", "Erreur de base de données : " . $e->getMessage());
    }
}